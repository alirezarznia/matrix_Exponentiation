

G(1) = 1
G(2) = 1
G(n) = aG(n-1) + bG(n-2) + c.


#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;

ll a, b ,c , n ,m  ;
ll arr[3][3];
ll arrt[3][3];
ll mult(ll q[][3]){
    ll v[3][3];
    Rep(i ,3){
        Rep(j ,3){
            ll qq=0;
            Rep(k ,3)
                 qq +=arr[i][k] * q[k][j];
            v[i][j]=qq%m;
        }
    }
    Rep(i ,3){
        Rep(j ,3) arr[i][j]=v[i][j];
}
    return 0;
}
ll F(ll n){
    if(n==1) return ((arr[0][0]+arr[0][1]+arr[0][2])%m);
    F(n/2);
    mult(arr);
    if(n%2) mult(arrt);
     return (arr[0][0]+arr[0][1]+arr[0][2])%m;
}
int main(){
  //  Test;
    ll t ;cin >>t;
    while(t--){
        cin >> a >>b>>c>>n>>m;
        arr[0][0]=a; arr[0][1]=b;arr[0][2]=c;
        arrt[0][0]=a; arrt[0][1]=b; arrt[0][2]=c;
        arr[1][0]=1; arr[1][1]=0;arr[1][2]=0;
        arr[2][0]=0; arr[2][1]=0;arr[2][2]=1;
        arrt[1][0]=1; arrt[1][1]=0;arrt[1][2]=0;
        arrt[2][0]=0; arrt[2][1]=0;arrt[2][2]=1;
        cout<<(F(n-2))%m<<endl;
    }
}