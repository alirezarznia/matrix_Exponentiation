spoj ADAMONEY - Ada and Economy


#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>
#include 	<cassert>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

#define MU 1000000007
#define MX 5
typedef long long ll;

using namespace std;

class matrix{
public:
    ll r , c;
    ll data[MX][MX];
    matrix(ll x ,ll y ){
        (this->r)= x;
        (this->c)= y;
        Set(data , 0);
    }
    inline matrix operator * (const matrix &a){
        matrix res(r , a.c);
        Rep(i ,r)
            Rep(j ,a.c)
                Rep(k ,c) {res.data[i][j]+=((data[i][k] * a.data[k][j])%MU); res.data[i][j]%=MU;}
        return res;
    }
};
matrix pw(matrix m , ll n){
    if(n==1) return m ;
    matrix p = pw(m , n/2);
    p=p*p;
    if(n&1) p=p*m;
    return p;
}
int main(){
    //Test;
    ll t ;cin>>t;
    matrix mx(5,5);
    mx.data[1][0]=1;mx.data[2][1]=1;mx.data[3][2]=1;mx.data[4][3]=1;mx.data[0][0]=1;mx.data[0][1]=2;
    mx.data[0][2]=0;mx.data[0][3]=5;mx.data[0][4]=1;
    while(t--){
        matrix A(5,1);
        Rep(i ,5){ll x; cin>>A.data[4-i][0];}
        ll n ; cin>>n;
        if(n<5) cout<<A.data[4-n][0]<<endl;
        else{
        matrix ans(5,1);ans= pw(mx ,n-4)*A;
        cout<<ans.data[0][0]<<endl;
        }
    }
    return 0;
}
